
package com.mycompany.arbolprueba;

import javax.swing.JOptionPane;


public class PruebaArbol {
    public static void main(String args [])
    {
        Arbol arbol = new Arbol();
        int valor;
        String Dato;
        
        System.out.println("Insertando valores siguientes: ");
        
        Dato = JOptionPane.showInputDialog("Por favor escribe los nodos que desees");
        int n = Integer.parseInt(Dato);
        for(int i = 1; i <= n; i++ )
        {
            Dato = JOptionPane.showInputDialog("seleccione el " + i + " para colocar en el Arbol");
            valor = Integer.parseInt(Dato);
            System.out.print(valor + " ");
            arbol.insertarNodo(valor);
        }
        
        System.out.println("\n\nRecorrido Preorden");
        arbol.recorridoPreorden();
        
        System.out.println("\n\nRecorrido Inorden");
        arbol.recorridoInorden();
        
        System.out.println("\n\nRecorrido Postorden");
        arbol.recorridoPosorden();
    }
    
}
